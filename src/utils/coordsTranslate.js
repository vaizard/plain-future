// Adapted from github.com/vasturiano/three-globe/tree/master/src/utils/coordTranslate.js
// Licenced under MIT. License bellow.

/*
MIT License

Copyright (c) 2019 Vasco Asturiano
          (c) 2020 Tomas Zelina

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

const GLOBE_RADIUS = 100

function polar2Cartesian(lat, lng, relAltitude = 0) {
    const phi = (90 - lat) * Math.PI / 180;
    const theta = (90 - lng) * Math.PI / 180;
    const r = GLOBE_RADIUS * (1 + relAltitude);
    return {
        x: r * Math.sin(phi) * Math.cos(theta),
        y: r * Math.cos(phi),
        z: r * Math.sin(phi) * Math.sin(theta),
        phi: phi,
        theta: theta
    };
}

function cartesian2Polar({ x, y, z }) {
    const r = Math.sqrt(x*x + y*y + z*z);
    const phi = Math.acos(y / r);
    const theta = Math.atan2(z, x);

    return {
        lat: 90 - phi * 180 / Math.PI,
        lng: 90 - theta * 180 / Math.PI - (theta < -Math.PI / 2 ? 360 : 0), // keep within [-180, 180] boundaries
        altitude: r / GLOBE_RADIUS - 1
    }
}

export { polar2Cartesian, cartesian2Polar };