import model from "./ar-config.json"
import Models from "./ARModels";
import ConsentScreen from "./ConsentScreen";
import React from "react";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            consent: false
        }
    }

    giveConsent(){
        this.setState({consent: true})
    }

    render(){
        if(!this.state.consent){
            return <ConsentScreen onEnabled={() => {this.giveConsent()}}/>
        }

        return <><Models models={model}/></>
    }
}

export default App;
