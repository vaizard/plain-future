import Poster from "./Poster";
import './Link.css';

const OutOfOrder = (props) => {
    return <Poster image="https://static.newfuture.industra.space/assets/arts/a95fe630-bb90-4b63-8742-aa016da69a99/cover.jpg">
        <div className="outOfOrder">Probíhá kurátorská úprava vystavovaného díla. Brzy bude opět k dispozici.</div>
    </Poster>
}

export default OutOfOrder;