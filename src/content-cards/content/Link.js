import Poster from "./Poster";
import './Link.css'

const Link = (props) => {
    return <Poster onClick={() => window.open(props.href, '_blank')} image={props.poster}>
        <a className="linkButton" href={props.href} target='_blank' rel="noreferrer" onClick={(event) => event.stopPropagation()}>{props.text ?? "Go to website"}</a>
    </Poster>
}

export default Link;