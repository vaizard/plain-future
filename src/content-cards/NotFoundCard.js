

const Error404 = () => {
    return <div>
        <h1>Well, this is embarrassing...</h1>
        <div>We've looked everywhere, but we can't find the page you are looking for. </div>
    </div>
}

export default Error404;