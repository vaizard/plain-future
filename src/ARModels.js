import React from "react";


class Model extends React.Component{
    render() {
        let models = [];
        let assets = {};
        for(let model of this.props.models) {
            let modelId = model.uuid;
            let barcode = model.barcode ?? null;
            let patternUri = model.pattern ?? null;
            let modelInfo = this.getModel(model);
            assets = {...assets, ...modelInfo.assets};


            let marker = <a-marker type={patternUri ? 'pattern' : 'barcode'} url={patternUri} value={barcode} key={"marker-" + modelId}>
                {modelInfo.model}
                <a-sphere position='0 0 0' scale="0.5 0.5 0.5" material='opacity: 0.5;'></a-sphere>
            </a-marker>;

            models.push(marker);
        }

        return <a-scene className="ar" embedded="true" arjs="detectionMode: mono_and_matrix; matrixCodeType: 3x3_PARITY65; debugUIEnabled: true;">
            {this.parseAssets(assets)}
            {models}

            <a-entity camera="active: true"></a-entity>
        </a-scene>

    }

    getModel(model) {
        if(model.gltf) {
            let assets = {};alert("#" + model.uuid);
            return {
                assets: assets,
                model: <a-entity scale="4 4 4" gltf-model={model.gltf}/>
            };

        }
        if (model.aframe) {
            alert("Not implemented yet!");
            return {
                assets: {},
                model: null
            };

        }
    }

    parseAssets(assets){
        let result = [];
        console.log(assets);
        console.log(Object.entries(assets))
        for(const [key, src] of Object.entries(assets)) {
            console.log([key, src]);
            result.push(<a-asset-item id={key} src={src} key={"asset-" + key} timeout="10000"></a-asset-item>);
        }
        return <a-assets>
            {result}
        </a-assets>;
    }
}

export default Model;
