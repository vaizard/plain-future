import {Component} from "react";
import CameraIcon from "./assets/camera_white.svg"
import "./ConsentScreen.css"


class ConsentScreen extends Component{


    render() {
        return <div className="consentScreen">
            <div className="ImageBackground"/>
            <div className="content">
                <div>Zažij umění jinak!</div>

                <div><button className="button" onClick={this.props.onEnabled} id="requestPermissions">Spustit rozšířenou realitu</button>
                </div>
                <div className="perminfo">
                    <img className="icon" alt="" src={CameraIcon}/> Pro spuštění je potřeba povolit přístup k fotoaparátu
                </div>

            </div>
        </div>
    }

}

export default ConsentScreen
