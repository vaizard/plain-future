import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './ar.css';
import reportWebVitals from './reportWebVitals';
import 'aframe';
import Script from 'react-load-script'
import ARApp from "./ARApp";

ReactDOM.render(
    <React.StrictMode>
        <Script url="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar.js" />
        <ARApp />
    </React.StrictMode>,
    document.querySelector("#root")
);

reportWebVitals();
